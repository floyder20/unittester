import unittest
from py1_AccountBalance import AccountBalance
from py1_AdditionGame import AdditionGame
from py1_RandomWalkOnGrid import RandomWalkOnGrid
from py1_ImprovingStatistics import ImprovingStatistics
from py1_CrossWord import CrossWord


class unittest_checker(unittest.TestCase):
    def test_AB(self):
        self.assertEqual(AccountBalance.processTransactions(100, ("C 1000", "D 500", "D 350")), 250)
        self.assertEqual(AccountBalance.processTransactions(-500, ("C 1000", "D 789")), -289)

    def test_AG(self):
        self.assertEqual(AdditionGame.getMaximumPoints(1, 1, 1, 8), 3)
        self.assertEqual(AdditionGame.getMaximumPoints(2,1,4, 12), 14)

    def test_RWG(self):
        self.assertEqual(RandomWalkOnGrid.getExpectation(1, 2, 3, 1, 2), 351)
        self.assertEqual(RandomWalkOnGrid.getExpectation(1, 4, 7, 1, 2), 501552)

    def test_IS(self):
        self.assertEqual(ImprovingStatistics.howManyGames(1000000000, 470000000), 19230770)

    def test_CW(self):
        self.assertEqual(CrossWord.countWords((".....X....X....", ".....X....X....", "..........X....", "....X....X.....", "...X....X....XX", "XXX...X....X...", ".....X....X....", ".......X.......", "....X....X.....", "...X....X...XXX", "XX....X....X...", ".....X....X....", "....X..........", "....X....X.....", "....X....X....."), 5), 8)




if __name__ == '__main__':
    unittest.main()
