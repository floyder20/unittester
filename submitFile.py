import argparse
import sys
from pat_core import testChecker
import logging



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--signature', type=str,
                        help = 'Enter the signature of your file')

    parser.add_argument('--filename', type=str,
                        help = 'Enter the name of your file')

    parser.add_argument('--loglevel', type = int, help = 'Enter logg level', default = 10)

    args = parser.parse_args()
    sig = args.signature
    filen = args.filename
    loglevel = args.loglevel

    # got all ArgumentParse
    # start logging

    logging.basicConfig(filename='sample.log', level=logging.DEBUG)

    logger = logging.getLogger('SubmitFile')

    logger.info('run started')

    #print('signatue and filename is ',sig, filen )
    try:

        p, f = testChecker(sig, filen )
        mesg = 'passed, failed = '  + str(p) + ' , ' + str(f)
        # p, f = testChecker(sig, filen )
        logger.debug(mesg)

    except IOError:
        logger.info('got exception')
        print('ERROR!!!')
        #raise




if __name__ == '__main__':
    main()
