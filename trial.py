import json

def add_puzzle(signature, filename, test_cases):

    try:
        os.stat(filename)
        os.stat(test_cases)



        puzzle_details = {'SIGNATURE' : signature, 'FILENAME': filename, 'TESTCASES': test_cases}
        puzzle_database = {signature : puzzle_details}
        with open('test.json') as f:
            data = json.load(f)

        data.update(puzzle_database)

        with open('test.json', 'w') as f:
            json.dump(data, f)

    except:
        print("ERROR : Files do not exist")


ret = add_puzzle('Swastini', 'aohnonotidasd', 'wDemierwe')
