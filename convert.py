import re
import os
import json
BASE_DIR = "."


def load(class_name,txt_file):
    """convert test file of assignment to json containing test cases input,output in dictionary with keyword cases """

    final_list = []
    FILE = open(txt_file, 'r')
    raw_data = FILE.read()
    p1 = ('System Test Results')
    p2 = ('Passed')
    txt_data = raw_data[raw_data.find(p1) +len(p1)+ 1 : raw_data.rfind(p2)+ len(p2)].strip()
    final_data = re.sub('\n+', '\n',  txt_data)
    final_data1 = final_data.replace('{', '[').replace('}', ']')
    list_str = final_data1.split('\n')
    case_number=0
    sol={}
    sol['cases']=[]
    path_to_data_sol=os.path.join(BASE_DIR,'data_sol')
    path_to_json_file=path_to_data_sol+"/"+class_name+".json"
    for item in list_str[1:]:
        value = item.strip().split('\t\t')
        v=value[0]
        output_value=value[1]
        p1="("+v+")"
        #input_value=eval(p1)
        input_value=value[0]

        case_number=case_number+1
        d = {"case number" : case_number, "parameters" : input_value, "expected" : output_value}
        sol['cases'].append(d)
    with open(path_to_json_file,'w') as json_data:    
        json.dump(sol,json_data) 
    return sol
