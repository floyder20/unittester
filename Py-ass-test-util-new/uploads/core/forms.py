from django import forms

from uploads.core.models import Document


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        signature = forms.CharField(max_length=100)
        fields = ('document', 'signature')
