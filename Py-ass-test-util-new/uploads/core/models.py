from __future__ import unicode_literals

from django.db import models


class Document(models.Model):
    document = models.FileField(upload_to='documents/')
    #sig_choices=[(x, str(x)) for x in range(1,25)]
    sig_choices=[('AB', 'AccountBalance.processTransactions'), ('AG', 'AdditionGame.sum'), ('CW', 'CrossWords.checkMatch')]
    signature = models.CharField(max_length=100, choices = sig_choices)
    uploaded_at = models.DateTimeField(auto_now_add=True)
