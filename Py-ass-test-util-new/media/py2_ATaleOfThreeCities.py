import math
class ATaleOfThreeCities():

        
    def distance(self, elementa, elementb, elementc, elementd):
            return math.sqrt((elementb-elementa)**2 + (elementd - elementc)**2)
        
    def connect(self, ax, ay, bx, by, cx, cy):
       
#       put your code here
        
        counta1 = 0
        countb2 = 0
        countc2 = 0
        distlist = [100, 100]
        for element1 in ax:
            countb1 = 0
            for element2 in bx:
                current_value = self.distance(element1, element2, ay[counta1], by[countb1])
                distlist.append(current_value)
                countb1 += 1
            counta1 += 1
        
        for element3 in bx:
            countc1 = 0
            for element4 in cx:
                current_value = self.distance(element3, element4, by[countb2], cy[countc1])
                distlist.append(current_value)
                countc1 += 1
            countb2 += 1
       
        for element5 in cx:
            counta2 = 0
            for element6 in ax:
                current_value = self.distance(element5, element6, cy[countc2], ay[counta2])
                distlist.append(current_value)
                counta2 += 1
            countc2 += 1
            
        distlist.sort()
        required_distance = distlist[0] + distlist[1]
        
        return required_distance
                


