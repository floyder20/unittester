import json
#import unittest
#import ast
import os
import importlib.util

import logging

logger = logging.getLogger('pat_core')

def getTestCases(signature):
    classname, method = signature.split('.')
    mesg = 'getTestCases, sig , class, method : ' + signature + ', ' + classname + ', ' + method
    logger.debug(mesg)
    file_name = "data_sol/" + classname + ".json"
    with open(file_name) as data_file:
        data = json.load(data_file)
        test_cases = data['cases']

    return(test_cases)


def testChecker(signature, filename):
    '''
    TODO: Fill up the documentation
    '''
    
    mesg = 'testChecker, signature, filename : ' + signature + ', ' + filename
    logger.debug(mesg)

    passed = 0
    failed = 0

    try:
        os.stat(filename)

    except IOError:
        logger.warning('File not Found')
        raise

    try:

        classname, method = signature.split('.')
        spec = importlib.util.spec_from_file_location('testfile', filename)
        mylib = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mylib)

        ex_class = 'mylib.' + classname + '()'
        my_class = eval(ex_class)

        test_cases = getTestCases(signature)


        for test1 in test_cases:
            param = '(' + test1['parameters'] + ')'
            ex_method = 'my_class.' + method + param
            p1 = eval(ex_method)
            p3 = eval(test1['expected'])

            if(p1 == p3):
                passed += 1
            else:
                failed += 1

        #print("Number of testcases passed:", passed)
        #print("Number of testcases failed:", failed)
        return( passed, failed)
    except ImportError:
        logger.warning('Got Import Error')
        raise





#ret = testChecker("AdditionGame.getMaximumPoints", "py1_AdditionGame.py")
                #p1, p2 = eval(test_cases['parameters'])
                #p3 = eval(test_cases['expected'])
