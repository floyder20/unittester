import json
import unittest
import ast
from py1_AccountBalance import AccountBalance
import os
import importlib.util

class unittest_checker(unittest.TestCase):


    def test_AB(self):
        class_name = "AccountBalance"
        method = "processTransactions"
        file_name = "data_sol/" + class_name + ".json"
        file_name2 = "py1_AccountBalance.py"
        os.stat(file_name)
        spec = importlib.util.spec_from_file_location('testfile', file_name2)
        mylib = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mylib)
        passed = 0
        failed = 0
        with open(file_name) as data_file:
            data = json.load(data_file)
            v = data['cases']
            for test_cases in v:
                p1, p2 = eval(test_cases['parameters'])
                p3 = eval(test_cases['expected'])

                if(AccountBalance.processTransactions(p1, p2) == eval(test_cases['expected'])):
                    passed += 1

                else:
                    failed += 1



                #self.assertEqual(AccountBalance.processTransactions(p1, p2), p3)

            print("Number of testcases passed:", passed)
            print("Number of testcases failed:", failed)







if __name__ == '__main__':
    unittest.main()
