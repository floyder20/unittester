import json
import unittest
import ast
import os
import importlib.util


def getTestCases(signature):
    classname, method = signature.split('.')
    file_name = "data_sol/" + classname + ".json"
    with open(file_name) as data_file:
        data = json.load(data_file)
        test_cases = data['cases']

    return(test_cases)


def testChecker(args):
    passed = 0
    failed = 0
    #os.stat(args.filename)

    classname, method = args.signature.split('.')
    spec = importlib.util.spec_from_file_location('testfile', args.filename)
    mylib = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mylib)

    ex_class = 'mylib.' + classname + '()'
    my_class = eval(ex_class)

    test_cases = getTestCases(args.signature)


    for test1 in test_cases:
        param = '(' + test1['parameters'] + ')'
        ex_method = 'my_class.' + method + param
        p1 = eval(ex_method)
        p3 = eval(test1['expected'])

        if(p1 == p3):
            passed += 1
        else:
            failed += 1

    print("Number of testcases passed:", passed)
    print("Number of testcases failed:", failed)
