class AdditionGame():
    def getMaximumPoints(self, A, B, C, N):
       list1 = [A, B, C]
       maximumPoints = 0
       for i in range(0, N):
           
           max_value = max(list1)
           maximumPoints += max_value
           max_index = list1.index(max_value)
           list1[max_index] -= 1
           if(list1[0] == 0 and list1[1] == 0 and list1[2] == 0):
               break
           
       return(maximumPoints)
