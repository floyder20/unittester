from django.shortcuts import render, redirect
from django.views.generic import TemplateView, ListView, CreateView
from django.core.files.storage import FileSystemStorage
from django.urls import reverse_lazy

from .forms import PuzzleForm
from .models import Puzzle


class Home(TemplateView):
    template_name = 'home.html'


def upload(request):
    context = {}
    if request.method == 'POST':
        uploaded_file = request.FILES['document']
        fs = FileSystemStorage()
        name = fs.save(uploaded_file.name, uploaded_file)
        context['url'] = fs.url(name)
    return render(request, 'upload.html', context)


def book_list(request):
    puzzles = Puzzle.objects.all()
    return render(request, 'book_list.html', {
        'Puzzles': puzzles
    })


def upload_book(request):
    if request.method == 'POST':
        form = PuzzleForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('book_list')
    else:
        form = PuzzleForm()
    return render(request, 'upload_book.html', {
        'form': form
    })


def delete_book(request, pk):
    if request.method == 'POST':
        puzzle = Puzzle.objects.get(pk=pk)
        puzzle.delete()
    return redirect('book_list')


class BookListView(ListView):
    model = Puzzle
    template_name = 'class_book_list.html'
    context_object_name = 'books'


class UploadBookView(CreateView):
    model = Puzzle
    form_class = PuzzleForm
    success_url = reverse_lazy('class_book_list')
    template_name = 'upload_book.html'
