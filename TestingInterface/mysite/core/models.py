from django.db import models


class Puzzle(models.Model):
    #title = models.CharField(max_length=100)
    signature = models.CharField(max_length=100)
    #author = models.CharField(max_length=100)
    Trainer = models.CharField(max_length=100)
    #pdf = models.FileField(upload_to='books/pdfs/')
    #cover = models.ImageField(upload_to='books/covers/', null=True, blank=True)
    puzzle_description = models.FileField(upload_to='books/pdfs/')
    test_cases = models.ImageField(upload_to='books/covers/', null=True, blank=True)


    def __str__(self):
        return self.title

    def delete(self, *args, **kwargs):
        self.puzzle_description.delete()
        self.test_cases.delete()
        super().delete(*args, **kwargs)
